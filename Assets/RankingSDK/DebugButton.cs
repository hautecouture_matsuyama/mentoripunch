﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugButton : MonoBehaviour {


    [SerializeField]
    int cnt;

    [SerializeField]
    int wait;

	public void DebugSignUp()
    {
        RankingManager.Instance.DebugSignUp(cnt, wait);
    }
}
