﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;
using NendUnityPlugin.AD;

public class PlayerController : MonoBehaviour {

	public static int Score;
    //public Canvas ShareCanvas;

	public GameObject[] GameOverWindow;
	public GameObject[] LeftrightButtons;
	public GameObject[] tapTap;

    public GameObject GameOverPanel;
    public GameObject RankingCanvas;


	//public UILabel Scorelabel;
	//public UILabel Levellabel;

	public Text[] ScoreTexts;
	public Text[] LevelTexts;
	public Text[] LevelString;

    public Text MyRank;

	public Sprite[] AnimationSprites;
	public int[] AnimSpriteFrameCount;

	public Sprite[] ExplosionSprite;

	private float numImages = 2;
	private bool hitleft = false;
	private bool hitRight = false;

	public Sprite[] hamSprite;
	public Sprite[] hamDamSprite;

	public GameObject Leaf;
	public GameObject Deastoyparticle;
	public int DeastoyparticleOrder;

	private int motion;

	private bool runOnce;
	public static bool OneTimeOnly;

	public GameObject FallingLeaves;
	public GameObject HitParticle;

	public GameObject HitEffect;
	public int HitEffectOrder;

	//public GameObject OpenShares;
	//public GameObject[] ShareButtons;

	public GameObject HamsTower;

	//public string shareBtnSpriteName;
	//public string shareCloseBtnSpriteName;

	public GameObject scorePanel;
	//public GameObject BannerGO;
	public GameObject MusicGO;

	public HealthBar nekoHP;

	public GameObject timeOverObject;

    //名前登録用Canvas
    public GameObject NameEditCanvas;


	public bool debugFlag = false;

	public float minTweenTime = 0.1f;

	[SerializeField]
	private float Min_minTweenTime = 0.02f;

	[SerializeField]
	private float limitHeightDist;

	[SerializeField]
	private GameObject[] enemies;

	[SerializeField]
	private int accelDownScore;

	[SerializeField]
	private GameObject explanation;

    [SerializeField]
    private GameObject[] ba;

    [SerializeField]
    private GameObject[] enemys;
	public float hightFixedCnst = 1f;
	public float heightLocalMax = 20f;

	public string URL_IOS;
	public string URL_Android;

	private float org_minTweenTime;

	float lastTouchTime;

	//private Queue SoccerBalls;
	List<GameObject> Hamsters;

	SpriteRenderer playerSprn;

	private bool posLeft = true;

    RankManager RM;
    public GameObject RM_obj;

    [SerializeField]
    private SpriteRenderer test;

	private Soundmanager smanager
	{
		get{
			if(_smanager == null){
				_smanager = 
					GameObject.Find("Audiomanager").GetComponent<Soundmanager>();
			}

			return _smanager;
		}
	}
	private Soundmanager _smanager;

	// Use this for initialization
	void Start () {
        PlayerPrefs.SetString("MYR","");

        RM = RM_obj.GetComponent<RankManager>();

        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            NendAdInterstitial.Instance.Load(542780, "6693fdf809f0994ac5570a89e298d83d42d27409");
        }
            
        else if(Application.platform == RuntimePlatform.Android)
        {
            NendAdInterstitial.Instance.Load(542773, "d514456adefd4b654723a88e4c3c49dea8afe370");
        }
            


        //Levellabel.enabled = false;

        foreach (Text txt in LevelTexts){
			txt.enabled = false;
		}
		foreach(Text txt in LevelString){
			txt.enabled = false;
		}

		OneTimeOnly = true;
		runOnce = true;
		if(gameoverCheck)
		{
			Time.timeScale = 1;

			gameoverCheck = false;

			foreach(GameObject lf in LeftrightButtons)
				lf.SetActive(true);

			foreach(GameObject chd in GameOverWindow)
				chd.SetActive(false);

			foreach(GameObject lf in tapTap)
				lf.SetActive(true);

		}else{

			Time.timeScale = 0;

			foreach(GameObject lf in LeftrightButtons)
				lf.SetActive(false);
			foreach(GameObject lf in tapTap)
				lf.SetActive(false);
		}

		InitHamsters ();

		if(!StartButtonScript.gamePlayed)
		{
			OnPlay();
		}

		if(debugFlag){
			//StartCoroutine(AutoPlayCoroutine());
			Time.timeScale = 0.05f;

		}

		lastTouchTime = Time.time;

		SpriteRenderer[] tmpSprn = transform.
			GetComponentsInChildren<SpriteRenderer>();
		foreach(SpriteRenderer sprn in tmpSprn){
			if(sprn != null){
				if(sprn.transform.parent == transform){
					playerSprn = sprn;
				}
			}
		}

		org_minTweenTime = minTweenTime;
	}

	public void InitHamsters()
	{
		SortedList<float, GameObject> SortedSoccerBalls = 
			new SortedList<float, GameObject> ();

		HamsTower.SetActive (true);
        
        //GameObject[] balls = GameObject.FindGameObjectsWithTag ("SoccerBall");
        foreach (GameObject go in ba){
			SortedSoccerBalls.Add(go.transform.position.y, go);

		}
        //SoccerBalls = new Queue ();
        Hamsters = new List<GameObject> ();
        for (int i=0; i<SortedSoccerBalls.Count; i++){
			float key = SortedSoccerBalls.Keys[i];
            
            SpriteRenderer sprn = 
				SortedSoccerBalls[key].GetComponent<SpriteRenderer>();
            //Debug.Log("55555" + sprn);
            sprn.sprite = hamSprite[Random.Range(0, hamSprite.Length)];
            //SoccerBalls.Enqueue(SortedSoccerBalls[key]);
            Hamsters.Add(SortedSoccerBalls[key]);
		}

        //Debug.Log("666666");
    }
	
	// Update is called once per frame
	void Update () {
		//Scorelabel.text = ""+Score;
		//Debug.Log("Updateは呼ばれています！！");



		foreach(Text txt in ScoreTexts){
			txt.text = ""+Score;
		}

		if(hitleft)
		{
			motion++;

			for(int i=0; i<AnimSpriteFrameCount.Length; i++)
			{
				int motion_min = (i == 0)? 0: AnimSpriteFrameCount[i-1];

				if(motion >= motion_min && motion < AnimSpriteFrameCount[i])
				{
					//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[i];
					playerSprn.sprite = AnimationSprites[i];
					break;
				}
			}

			if(motion >= AnimSpriteFrameCount[AnimSpriteFrameCount.Length - 1])
			{
				//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];
				playerSprn.sprite = AnimationSprites[0];
				
				motion = 0;
				hitleft = false;

			}

			if(gameoverCheck){
				//this.transform.GetComponent<SpriteRenderer>().sprite = ExplosionSprite[0];
				playerSprn.sprite = ExplosionSprite[0];
			}
		}else if(hitRight)
		{
			motion++;

			for(int i=0; i<AnimSpriteFrameCount.Length; i++)
			{
				int motion_min = (i == 0)? 0: AnimSpriteFrameCount[i-1];
				
				if(motion >= motion_min && motion < AnimSpriteFrameCount[i])
				{
					//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[i];
					playerSprn.sprite = AnimationSprites[i];
					break;
				}
			}
			
			if(motion >= AnimSpriteFrameCount[AnimSpriteFrameCount.Length - 1])
			{
				//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];
				playerSprn.sprite = AnimationSprites[0];
				
				motion = 0;
				hitRight = false;
				
			}

			if(gameoverCheck){
				//this.transform.GetComponent<SpriteRenderer>().sprite = ExplosionSprite[0];
				playerSprn.sprite = ExplosionSprite[0];
			}
		}


		if (Input.GetKeyUp(KeyCode.Escape))
		{
			if(StartButtonScript.gamePlayed){

				//GameObject banner = GameObject.FindWithTag ("banner");

				if(!GameOverWindow[0].activeSelf)
				{
					StartButtonScript.gamePlayed = false;
					Application.LoadLevel(Application.loadedLevel);
					/*if(banner != null){
						banner.GetComponent<RectTransform>().anchoredPosition = 
							Vector2.zero;
					}*/
					BannerCtrl.component.SetBanner();
				}
				else
				{
					gameOver go = GameOverWindow[0].GetComponent<gameOver>();

					if(go.isShareIconsOpend()){
						go.ShareButtonClicked();
					}else{
						StartButtonScript.gamePlayed = false;
						Application.LoadLevel(Application.loadedLevel);
						/*if(banner != null){
							banner.GetComponent<RectTransform>().anchoredPosition = 
								Vector2.zero;
						}*/
						BannerCtrl.component.SetBanner();
					}

				}

			}else{

			}

		}
	}

	IEnumerator AutoPlayCoroutine()
	{

		//float waitTime = 0.2f;

		float distanceForChanging = 3.1357f;

		int waitCount = 10;

		bool left = true;

		while(true){

			//yield return new WaitForSeconds(waitTime);

			for(int i=0; i<waitCount / Time.timeScale; ++i){
				yield return null;
			}

			//GameObject[] enemyObjects = 
			//	GameObject.FindGameObjectsWithTag("Enemy");

			if(transform.position.x < 0){

				foreach(GameObject go in enemys){
					
					float y_dist = Mathf.Abs(
						go.transform.position.y - transform.position.y);

					int sign = (int)Mathf.Sign(go.transform.position.x);

					if(y_dist < distanceForChanging && sign == -1 && left)
					{
						Moveright();	//1
						left = false;
						break;
					}
					
				}
				
				if(left){
					Moveleft();	//2

				}
			}else{

				foreach(GameObject go in enemys)
                {

					float y_dist = Mathf.Abs(
						go.transform.position.y - transform.position.y);

					int sign = (int)Mathf.Sign(go.transform.position.x);

					if(y_dist < distanceForChanging && sign == 1 && !left)
					{
						Moveleft();	//3
						left = true;
						break;
					}

				}

				if(!left){
					Moveright();	//4

				}
			}

		}

	}

	void Moveleft()
	{
		if(explanation.activeInHierarchy){
			return;
		}

		if(runOnce)
		{
			runOnce = false;
			//healthBar.enabled = true;
			nekoHP.enabled = true;

			foreach(GameObject lf in tapTap)
				lf.SetActive(false);


		}

		if(Time.time - lastTouchTime < minTweenTime){
			return;
		}else{
			lastTouchTime = Time.time;
		}

		this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];

		if (!posLeft/*transform.localPosition.x > 0*/) 
		{
			posLeft = true;

			transform.localScale = new Vector3(-transform.localScale.x, 
			                                   transform.localScale.y, 
			                                   transform.localScale.z);

			transform.DOMove(new Vector3 (-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z), minTweenTime);

			bool soundOn = true;

			foreach(GameObject go in enemies){
				if(go.activeInHierarchy){
					if(Mathf.Abs(go.transform.position.y - transform.position.y) < limitHeightDist){
						soundOn = false;
						break;
					}
				}

			}

			if(soundOn)
				smanager.PlayMoveSound(0);

		}
		else
		{
			smanager.PlayOneShotHitSound();

			//UpdateHealthbar();

			//healthBar.IncreaseHealthBar();
			nekoHP.IncreaseHealthBar();

			StartCoroutine(KickSoccerBall(1));

			MoveBackground.Slide1point = true;

			hitleft = true;
			motion = 0;
		//	this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];

			Vector3 forward = transform.TransformDirection(Vector3.right) * 10;
			Debug.DrawRay(transform.position, forward, Color.white);
			
		}

	}

	IEnumerator FadeOutSprite(SpriteRenderer sprn)
	{
		float fadeSpeed = 2;
		float explodeSpeed = 3;
		float scaleMax = 1.5f;

		while(sprn.color.a > 0){
			if(sprn.color.a - fadeSpeed*Time.deltaTime < 0){
				break;
			}else{
				sprn.color -= new Color(0, 0, 0, fadeSpeed*Time.deltaTime);

				if(sprn.transform.localScale.x < scaleMax){
					sprn.transform.localScale += 
						new Vector3(explodeSpeed*Time.deltaTime, 
						            explodeSpeed*Time.deltaTime, 0);
				}

				yield return null;
			}

		}

		Destroy (sprn.gameObject);
	}

	/*void UpdateHealthbar()
	{
		//HealthBar helthBar = GameObject.Find("Progress Bar").GetComponent<HealthBar>();
		
		//helthBar.healthslider.value += Time.deltaTime*3f;
		healthBar.health += 0.3f;
	}*/

	void Moveright()
	{
		if(explanation.activeInHierarchy){
			return;
		}

		if(runOnce)
		{
			runOnce = false;
			//healthBar.enabled = true;
			nekoHP.enabled = true;

			foreach(GameObject lf in tapTap)
				lf.SetActive(false);
		}

		if(Time.time - lastTouchTime < minTweenTime){
			return;
		}else{
			lastTouchTime = Time.time;
		}

		//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[3];
		this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];

		if (posLeft/*transform.localPosition.x < 0*/) 
		{
			posLeft = false;

			transform.localScale = new Vector3(-transform.localScale.x, 
			                                   transform.localScale.y, 
			                                   transform.localScale.z);

			transform.DOMove(new Vector3 (-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z), minTweenTime);

			bool soundOn = true;
			
			foreach(GameObject go in enemies){
				if(go.activeInHierarchy){
					if(Mathf.Abs(go.transform.position.y - transform.position.y) < limitHeightDist){
						soundOn = false;
						break;
					}
				}
				
			}

			if(soundOn)
				smanager.PlayMoveSound(0);
		}
		else
		{
			smanager.PlayOneShotHitSound();
			hitRight = true;
			motion = 0;

			//healthBar.IncreaseHealthBar();
			nekoHP.IncreaseHealthBar();

			StartCoroutine(KickSoccerBall(-1));

			MoveBackground.Slide1point = true;
		}
	}

	public void SetMinTweenTime(int preLevel, int maxLevel)
	{
		minTweenTime = org_minTweenTime
			- (org_minTweenTime - Min_minTweenTime) * preLevel / maxLevel;
	}

	int GetLowHamIndex(List<GameObject> gos)
	{
		int index = 0;
		float minHeight = gos [0].transform.position.y;

		for(int i=0; i<gos.Count; ++i){
			if(gos[i].transform.position.y < minHeight){
				index = i;
				minHeight = gos[i].transform.position.y;
			}
		}

		return index;
	}

	//dir -- ボールを蹴る方向（1,-1）
	IEnumerator KickSoccerBall(int dir)
	{
		int counter = 0;

		int waitCount = AnimSpriteFrameCount[0]
			//(AnimSpriteFrameCount[0] + AnimSpriteFrameCount[1]) / 2
				;

		while(counter < waitCount){
			counter++;
			yield return null;
		}
		
		Vector3 ballPosition = new Vector3(0
		                                   , this.transform.localPosition.y - .4f
		                                   ,this.transform.localPosition.z);
		
		GameObject leafthrow = Instantiate(Leaf, ballPosition + dir * 0.5f * Vector3.right,
		                                   Quaternion.identity//Quaternion.Euler(0, 0, 85)
		                                   ) as GameObject;

		//ダメージスプライトのセット
		GameObject hamGo = //(GameObject)SoccerBalls.Peek ()
			Hamsters[GetLowHamIndex(Hamsters)];
		SpriteRenderer hamSprn = hamGo.GetComponent<SpriteRenderer>();
		for(int i=0; i<hamSprite.Length; i++){
			if(hamSprite[i] == hamSprn.sprite){
				SpriteRenderer hamDamSprn = leafthrow.GetComponent<SpriteRenderer>();
				hamDamSprn.sprite = hamDamSprite[i];
			}
		}

		leafthrow.GetComponent<Rigidbody2D>().AddForce(new Vector3(dir*300,Random.Range(0, 300),0));

		leafthrow.transform.localScale = 
			new Vector3 (dir * leafthrow.transform.localScale.x, 
			             leafthrow.transform.localScale.y, 
			             leafthrow.transform.localScale.z);

		leafthrow.GetComponent<Rigidbody2D>().AddTorque(Random.Range (-300, 300));
		
		GameObject hitEffectObject = Instantiate(
			HitEffect, ballPosition, 
			Quaternion.Euler(0, 0, 360*Random.value)) as GameObject;

		foreach(ParticleSystem ps in hitEffectObject
		        .GetComponentsInChildren<ParticleSystem>())
		{
			ps.GetComponent<Renderer>().sortingOrder = HitEffectOrder;

		}

		//StartCoroutine(
		//	FadeOutSprite(hitEffectObject.GetComponent<SpriteRenderer>()));
		
		ResetSoccerBalls();

	}

	void ResetSoccerBalls(){
		int ballCount = //SoccerBalls.Count
			Hamsters.Count;

		int index = GetLowHamIndex (Hamsters);
		GameObject ball = //(GameObject)SoccerBalls.Dequeue ()
			Hamsters[index];

		Hamsters.RemoveAt (index);

		ball.GetComponent<SpriteRenderer>().sprite = 
			hamSprite[Random.Range(0, hamSprite.Length)];

		float highestHeight = ball.transform.position.y;
		foreach(Object obj in //SoccerBalls
		        Hamsters){
			GameObject go = (GameObject)obj;
			if(highestHeight < go.transform.position.y){
				highestHeight = go.transform.position.y;
			}
		}

		ball.transform.localPosition = 
			new Vector3 (0, 11, 0);

		Hamsters.Add (ball);
	}

	private static bool gameoverCheck = false;

	void OnTriggerEnter2D (Collider2D c)
	{

		if (c.tag == "Enemy") {
			//FallingLeaves.SetActive(true);	サッカーなので葉っぱが降るのはおかしい

			Vector3 EffectPosition = 
				(0.75f * c.transform.position + 1.25f * transform.position) / 2.0f;

			GameObject particle = Instantiate (Deastoyparticle, 
				                      EffectPosition,
				                      Quaternion.Euler (90, 0, 0)) as GameObject;

			ParticleSystem[] particleSystems = 
				particle.GetComponentsInChildren<ParticleSystem> ();

			foreach (ParticleSystem ps in particleSystems) {
				ps.GetComponent<Renderer> ().sortingOrder = DeastoyparticleOrder;
			}

			GameOver (0);

			if (Random.value < 0.33f) {
				StartCoroutine (ShowInterstitial ());
			}

		}
	}
		
    //全面広告はここに！！！！！！！！！
	IEnumerator ShowInterstitial()
    {
		yield return new WaitForSeconds (2);

		NendUnityPlugin.AD.NendAdInterstitial.Instance.Show ();
	}
    /*
	IEnumerator ShowGameOverPanel()
    {
		yield return new WaitForSeconds (2.5f);

        //自分の順位取得
        //RankingManager.Instance.GetRankingData ();
        InvokeOver();
        if (GameObject.Find ("BannerButton") != null) {
			GameObject.Find ("BannerButton").GetComponent<BannerCtrl> ().SetBanner ();
		}
        if (NendAdInterstitial.Instance != null)
        {
            NendAdInterstitial.Instance.Show(542780);
        }
        GameOverPanel.SetActive(true);

    }
    */
    private void GetRank()
    {
        //RankingManager.Instance.GetMyRankingDate();
        //string myranks = PlayerPrefs.GetString("MYRANK");

    }

    void RankUpdate()
    {
        //RankingManager.Instance.GetMyRankingDate();
        string myrank = PlayerPrefs.GetString("MYR");
        MyRank.text = myrank + "位";
    }
   


	public void GameOver(int type)
	{
       
        PlayerPrefs.SetInt("fastScore", Score);
        RM.SetRanking(Score);

        
        //if (RankingManager.Instance.CheckAccount())
        //{
        //    //スコア送信
        //    RankingManager.Instance.SendRanking(Score);

        //    RankingManager.Instance.GetMyRankingDate();

        //    StartCoroutine(RankingCheaker());
        //    i = 0;
        //}

        //StartCoroutine (ShowGameOverPanel ());

        Invoke("InvokeGameOver", 5f);

		//接続されているときの処理
		//Invoke ("ShowInterstitial", 1.25f);

        //ShareCanvas.gameObject.SetActive(true);
		//this.gameObject.GetComponent<SpriteRenderer>().enabled = false;

		//SpriteRenderer playerSprite = 
		//	gameObject.GetComponent<SpriteRenderer> ();
		if (transform.position.x < 0) {
			playerSprn.sprite = ExplosionSprite[0];
		} else {
			playerSprn.sprite = ExplosionSprite[1];
		}

		gameoverCheck = true;

		smanager.StopMoveSound ();
		smanager.StopMusic();
		
		smanager.PlayGameoverSound();

		//GameObject progessbar = GameObject.Find("Progress Bar");
		//progessbar.GetComponent<HealthBar>().enabled = false;
		//progessbar.SetActive(false);

		//healthBar.enabled = false;
		nekoHP.enabled = false;
		//healthBar.gameObject.SetActive (false);
		//nekoHP.gameObject.SetActive (false);

		scorePanel.SetActive (false);

		/*GameObject banner = GameObject.FindWithTag ("banner");
		if (banner != null) {
			banner.GetComponent<RectTransform>().anchoredPosition = 
				Vector2.zero;
		}*/
		//BannerCtrl.component.SetBanner ();

		MusicGO.SetActive (true);

		if(Leaf != null)
		{
			foreach(GameObject lef in GameObject.FindGameObjectsWithTag("leaf"))
			Destroy(lef);
		}
		//Time.timeScale = 0;

		timeOverObject.SetActive (true);


        /*ここにゲームオーバー追加*/

		Text timeOverText = timeOverObject.GetComponent<Text>();

		if(type == 0){
			//hit
			timeOverText.text = "";


		}else{
			//time over
			timeOverText.text = "じかんぎれ";
		}

		/*foreach (GameObject chd in GameOverWindow) {
			chd.SetActive (true);
		}*/

		/*
		GameOverWindow[1].SetActive(false);
		*/
		foreach(GameObject lf in LeftrightButtons)
			lf.SetActive(false);


		/*
#if UNITY_IPHONE
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			ReportScore ( Score, "com.timberman.ts");
#endif
		*/

		foreach(Text txt in ScoreTexts){
			txt.enabled = false;
		}
		foreach(Text txt in LevelTexts){
			txt.enabled = false;
		}
		foreach(Text txt in LevelString){
			txt.enabled = false;
		}
	}

    private void InvokeGameOver()
    {
        InvokeOver();
        if (GameObject.Find("BannerButton") != null)
        {
            GameObject.Find("BannerButton").GetComponent<BannerCtrl>().SetBanner();
        }
        if (NendAdInterstitial.Instance != null)
        {
            if(Application.platform == RuntimePlatform.IPhonePlayer)
            NendAdInterstitial.Instance.Show(542780);
            else if(Application.platform == RuntimePlatform.Android)
            {
                NendAdInterstitial.Instance.Show(542773);
            }
        }
        GameOverPanel.SetActive(true);
    }

    private void InvokeOver()
    {
        RM.InvokeOver();
    }

    public void OnTransitionURL()
	{
		string transitionLink;

		#if UNITY_ANDROID
		transitionLink = URL_Android;
		#elif UNITY_IOS
		transitionLink = URL_IOS;
		#endif

		if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            //Application.OpenURL(transitionLink);
        }
			
	}

	public void OnPlay()
	{
        //ShareCanvas.gameObject.SetActive(false);
		Score = 0;
		Time.timeScale = 1;

		//this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

		if(gameoverCheck)
		{

			Application.LoadLevel(Application.loadedLevel);

		}else{
			foreach(GameObject chd in GameOverWindow)
				chd.SetActive(false);

			foreach(GameObject lf in LeftrightButtons)
				lf.SetActive(true);

			foreach(GameObject lf in tapTap)
				lf.SetActive(true);

		}

		StartButtonScript.gamePlayed = true;
	}

	private int a = 1;

	void LateUpdate()
	{
		int oneLevelScores = 20;

		if(Score > accelDownScore){
			oneLevelScores = 50;
		}

		if(Score % oneLevelScores == 0 && Score > 1 && OneTimeOnly)
		{
			//healthBar.HealthBarSpeedUp();
			nekoHP.HealthBarSpeedUp();
				OneTimeOnly = false;
				a++;
				StartCoroutine("ShowLevel");
		}
	}

	IEnumerator ShowLevel()
	{
		foreach(Text txt in LevelString){
			txt.enabled = true;
		}

		//Levellabel.enabled = true;
		//Levellabel.text = "Level " + a;
		foreach(Text txt in LevelTexts){
			txt.enabled = true;
			txt.text = //"レベル " + 
				a.ToString();
		}

		yield return new WaitForSeconds(2.0f);
		//Levellabel.enabled = false;
		foreach (Text txt in LevelTexts) {
			txt.enabled = false;
		}

		foreach(Text txt in LevelString){
			txt.enabled = false;
		}

		StopCoroutine("ShowLevel");

	}



	void ProcessAuthentication (bool success) {
		if (success) {
			Debug.Log ("Authenticated, checking achievements");
			
			// Request loaded achievements, and register a callback for processing them
			Social.ShowLeaderboardUI();
		}
		else
			Debug.Log ("Failed to authenticate");
	}

	void ReportScore (long score, string leaderboardID) {
		Debug.Log ("Reporting score " + score + " on leaderboard " + leaderboardID);
		Social.ReportScore (score, leaderboardID, success => {
			Debug.Log(success ? "Reported score successfully" : "Failed to report score");
		});
	}



    public Texture2D shareTexture;

    public void ShareTwitter()
    {
		string storeURL = "";
		#if UNITY_ANDROID
		storeURL = "http://hautecouture.jp/hautecouture_services/game/minigame/mentori_punch";
		#elif UNITY_IOS
		storeURL = "http://hautecouture.jp/hautecouture_services/game/minigame/mentori_punch";
		#endif

		int score = Score;
		string msg = "めんとりパンチで面倒だがトリあえず、にせめんトリを" + score + "匹やっつけた！\n";
		SocialConnector.SocialConnector.Share (msg,storeURL);
    }

    public void ShareLINE()
    {
		string storeURL = "";
		#if UNITY_ANDROID
        storeURL = "http://hautecouture.jp/hautecouture_services/game/minigame/mentori_punch";
		#elif UNITY_IOS
		storeURL = "http://hautecouture.jp/hautecouture_services/game/minigame/mentori_punch";
#endif

        int score = Score;
        string msg = "めんとりパンチで面倒だがトリあえず、にせめんトリを" + score + "匹やっつけた！\n"
			+ storeURL;
        string url = "http://line.me/R/msg/text/?" + //WWW.EscapeURL(msg)
			System.Uri.EscapeUriString(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
		string storeURL = "";
		#if UNITY_ANDROID
		storeURL = "http://hautecouture.jp/hautecouture_services/game/minigame/mentori_punch";
		#elif UNITY_IOS
		storeURL = "http://hautecouture.jp/hautecouture_services/game/minigame/mentori_punch";
		#endif

		int score = Score;
		string msg = "めんとりパンチで面倒だがトリあえず、にせめんトリを" + score + "匹やっつけた！\n";
		SocialConnector.SocialConnector.Share (msg,storeURL);
    }


}
