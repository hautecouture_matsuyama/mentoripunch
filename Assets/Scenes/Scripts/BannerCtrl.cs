﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BannerCtrl : MonoBehaviour {

	static public BannerCtrl component;

	[SerializeField]
	private Vector2 hiddenBannerPos = 500 * Vector2.up;

    private Vector2 SetBannerPos = 350 * Vector2.down;

    public Vector2 banner_pos;

	private RectTransform bannerTrfm{
		get{
			if(_bannerTrfm == null){
				_bannerTrfm = gameObject.GetComponent<RectTransform>();
			}
			return _bannerTrfm;
		}
	}
	private RectTransform _bannerTrfm;

	// Use this for initialization
	void Start () {

			DontDestroyOnLoad (transform.parent);

			component = this;

        banner_pos = new Vector2(-115,-97);



		Application.LoadLevel ("Scene");
	}
	
	// Update is called once per frame

	public void SetBanner()
	{
		bannerTrfm.anchoredPosition = banner_pos;
	}

	public void HideBanner()
	{
		bannerTrfm.anchoredPosition = hiddenBannerPos;
	}
}
