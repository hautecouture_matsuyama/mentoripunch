﻿using UnityEngine;
using System.Collections;

public class ResizeBackground : MonoBehaviour {

	[SerializeField]
	SpriteRenderer background;

	[SerializeField]
	GameObject[] x_scaled_objects; 

	[SerializeField]
	int orgScrWidth;

	[SerializeField]
	int orgScrHeight;

	// Use this for initialization
	void Start () {
		float scale = 1;

		if (orgScrHeight / (float)orgScrWidth > 
			Screen.height / (float)Screen.width) {

			scale = orgScrHeight * Screen.width / 
				(float)(orgScrWidth * Screen.height);
		} else {

			scale = orgScrWidth * Screen.height / 
				(float)(orgScrHeight * Screen.width);
		}

		background.transform.localScale = 
			new Vector3 (background.transform.localScale.x * scale, 
			             background.transform.localScale.y * scale, 
			             background.transform.localScale.z);

		foreach(GameObject go in x_scaled_objects)
		{
			if(go != null){
				go.transform.localScale = 
					new Vector3 (go.transform.localScale.x * scale, 
					             go.transform.localScale.y, 
					             go.transform.localScale.z);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
