﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveInfo : MonoBehaviour
{
    public static void SaveAllInfo()
    {       
        PlayerPrefs.SetString("PLAYERNAME", GameManager.instance.name_server);
        PlayerPrefs.SetInt("LOGININT", GameManager.instance.login_int);
        PlayerPrefs.SetString("USERID", GameManager.instance.userid_server);
    }
}
