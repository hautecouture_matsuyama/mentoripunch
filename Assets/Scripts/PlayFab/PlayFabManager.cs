﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;

public class PlayFabManager : MonoBehaviour
{
    RankManager RM;
    public GameObject RM_obj;

    //PlayerStatus b_Player_state; 

    public InputField EmailInput, PasswordInput, UsernameInput, Input_1, Input_2, Input_3;
    public Text LogText, StatText, LogTextString;
    public string myID;
    private int rank_position_int = 0;

    public int login_int = 0;
    public string name_server;
    public int bestScore;
    public int result_rank = 0;

    [SerializeField] Text[] RankTxt;
    [SerializeField] Text[] RankInt;
    [SerializeField] Text[] RankScore;

    //UIManager UI;
    public GameObject ui_obj;

    public bool getEnemy = false;
    public int enemySetAct_Int = 0;

    public int dev_Int = 0;



    private void Start()
    {
        //LoginNormal();

        // b_Player_state = GetComponent<PlayerStatus>();
        //UI = ui_obj.GetComponent<UIManager>();
        RM = RM_obj.GetComponent<RankManager>();

    }

    public void LoginBtn()
    {
        var request = new LoginWithCustomIDRequest { CustomId = GameManager.instance.userid_server };
        PlayFabClientAPI.LoginWithCustomID(request, (result) => GetStat(), (error) => LogText.text = "LogIn_Error");
    }

    public void GetStat()
    {
        PlayFabClientAPI.GetPlayerStatistics(
            new GetPlayerStatisticsRequest(),
            (result) =>
            {
                foreach (var eachStat in result.Statistics)
                {
                    //int level_int = eachStat.Value % 10;
                    int rank_int = eachStat.Value;
                    bestScore = rank_int;
                    GameManager.instance.bestScore = bestScore;
                    Debug.Log("GetStat_bestscore" + bestScore);
                }
                GetLeaderboardMain();
            }, (error) => Debug.Log("get_fail"));
    }

    public void RegisterBtn()
    {
        var request = new RegisterPlayFabUserRequest { Email = EmailInput.text, Password = PasswordInput.text, Username = UsernameInput.text, DisplayName = UsernameInput.text };
        PlayFabClientAPI.RegisterPlayFabUser(request, (result) => LogText.text = "Register_Success", (erroe) => LogText.text = "Register_Fail");
    }
    
    private string UserIdSet()
    {
        Guid guid = Guid.NewGuid();
        string userid = guid.ToString();
        GameManager.instance.userid_server = userid;
        SaveInfo.SaveAllInfo();
        return userid;
    }


    
    public void LoginMain(string name)
    {
        string user_id = UserIdSet();
        string name_LoginMain = name;
        var request = new LoginWithCustomIDRequest
        {
            CustomId = user_id,
            CreateAccount = true

        };
        PlayFabClientAPI.LoginWithCustomID(request, (result) => UpdateUserName(name_LoginMain), (error) => Debug.Log("LogIn_Error"));
        
    }


    public void LoginNormal()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = GameManager.instance.userid_server,
            CreateAccount = true
        };
        PlayFabClientAPI.LoginWithCustomID(request, (result) => GetStat(), (error) => Debug.Log("LogIn_Error"));
        //UI.anima.SetBool("showStart", true);
    }

    public void SetRank(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate { StatisticName = "ScorePunch", Value = score},
                //new StatisticUpdate { StatisticName = "Level", Value = GameManager.instance.player_level}
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, (result) => GetLeaderboardMain(), (error) => Debug.Log("Score_Save_Fail"));
    }

    public void UpdateUserName(string name)
    {
        //ユーザ名を指定して、UpdateUserTitleDisplayNameRequestのインスタンスを生成
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = name
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, (result) => GetStat(), (error) => Debug.Log("name_save_Error"));


    }

    public void GetLeaderboardMain()
    {
        var request = new GetLeaderboardRequest { StartPosition = 0, StatisticName = "ScorePunch", MaxResultsCount = 60};
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, (error) => print("Load_Fail"));
    }

    private void OnLeaderboardGet(GetLeaderboardResult result)
    {

        foreach (var item in result.Leaderboard)
        {

            rank_position_int = item.Position;
            //int level_int = item.StatValue % 10;
            int rank_int = item.StatValue;
            //string str_link = item.Position + 1 + " " + item.DisplayName + " " + rank_int;         
            int rank_pos = item.Position + 1;
            InputTextRank(item.DisplayName, rank_pos,rank_int);
            Debug.Log(item.Position + 1 + " " + item.DisplayName + " " + item.StatValue);



        }
    }

    private void InputTextRank(string rankname,int rank_pos ,int rank_int)
    {
        for (int i = 0; i < 50; i++)
        {
            if (i == rank_position_int && i < 20)
            {
                RankTxt[i].text = rankname;
                RankInt[i].text = rank_pos.ToString();
                RankScore[i].text = rank_int.ToString();
                //RankImg[i].gameObject.SetActive(true);
                //RankImg[i].sprite = InputImgRank(level);

                

                if (rankname == GameManager.instance.name_server)
                {
                    result_rank = rank_pos;
                    Debug.Log("result" + rank_pos + " " + rankname);
                    RM.currentRank_txt.text = result_rank.ToString();
                }          
            }

            else if (i == rank_position_int && i >= 20 && rankname == GameManager.instance.name_server)
            {
                result_rank = rank_pos;
                RM.currentRank_txt.text = result_rank.ToString();
            }
        }
    }

    /*
    private Sprite InputImgRank(int rank)
    {
        if(rank != 0) currentImg = LevelImg[rank - 1];
        else currentImg = LevelImg[0];


        return currentImg;
    }
    */













    /*
    public void SetLevel(int level)
    {
        var request = new UpdatePlayerStatisticsRequest { Statistics = new List<StatisticUpdate> { new StatisticUpdate { StatisticName = "Level", Value = GameManager.instance.player_level } } };
        PlayFabClientAPI.UpdatePlayerStatistics(request, (result) => Debug.Log("Score_Save_Success"), (error) => Debug.Log("Score_Save_Fail"));

    }
    */


    public void SetStat()
    {
        PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate{StatisticName = "Input_1",Value= int.Parse(Input_1.text)},
                new StatisticUpdate{StatisticName = "Input_2",Value= int.Parse(Input_2.text)},
                new StatisticUpdate{StatisticName = "Input_3",Value= int.Parse(Input_3.text)},
            }

        },
        (result) => { StatText.text = "Save_Success"; },
        (error) => { StatText.text = "Save_Fail"; });
    }
    /*

    public void GetStat()
    {
        PlayFabClientAPI.GetPlayerStatistics
        (
            new GetPlayerStatisticsRequest(),
            (result) =>
            {
                StatText.text = "";
                foreach (var eachStat in result.Statistics)
                    StatText.text += eachStat.StatisticName + " : " + eachStat.Value + "\n";

            },
            (error) => { StatText.text = "Fail"; }

        );
    }
    */

    public void SetDataString()
    {
        var request = new UpdateUserDataRequest() { Data = new Dictionary<string, string>() { { "A", "AA" }, { "B", "BB" } } };
        PlayFabClientAPI.UpdateUserData(request, (reuslt) => print("データセーブ成功"), (error) => print("データセーブ失敗"));
    }

    public void GetDataString()
    {
        var request = new GetUserDataRequest() { PlayFabId = myID };
        PlayFabClientAPI.GetUserData(request, (reuslt) => { foreach (var eachData in reuslt.Data) LogTextString.text += eachData.Key + " : " + eachData.Value.Value + "\n"; }, (error) => print("データロード失敗"));
    }

    public void GetLeaderboard()
    {
        LogText.text = null;
        var request = new GetLeaderboardRequest { StartPosition = 0, StatisticName = "ScorePunch", MaxResultsCount = 20, ProfileConstraints = new PlayerProfileViewConstraints() { ShowLocations = true, ShowDisplayName = true } };
        PlayFabClientAPI.GetLeaderboard(request, (result) =>
        {
            for (int i = 0; i < result.Leaderboard.Count; i++)
            {
                var curBoard = result.Leaderboard[i];
                LogText.text += curBoard.Profile.Locations[0].CountryCode.Value + " / " + curBoard.DisplayName + " / " + curBoard.StatValue + "\n";
            }
        },
        (error) => print("Load_Fail"));
    }



    [ContextMenu("クラウドスクリプト実行")]
    void StartCloudScript()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "helloWorld",
            FunctionParameter = new { text = "ヤッハロー！" },
            GeneratePlayStreamEvent = true
        }, OnCloudHelloWorld, (error) => Debug.Log("ERROR"));
    }

    void OnCloudHelloWorld(ExecuteCloudScriptResult result)
    {
        JsonObject jsonResult = (JsonObject)result.FunctionResult;
        jsonResult.TryGetValue("messageValue", out object messageValue);
        Debug.Log((string)messageValue);
    }
    [ContextMenu("クラウドスクリプトデータセーブ")]
    void StartSetData()
    {
        PlayFabClientAPI.ExecuteCloudScript(
        new ExecuteCloudScriptRequest
        {
            FunctionName = "SetData",
            FunctionParameter = new { onlineState = true },
            GeneratePlayStreamEvent = true
        },
        null, null);
    }
}
