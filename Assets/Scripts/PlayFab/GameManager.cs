﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region instance
    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
        LoadInfo.LoadAllInfo();
        SaveInfo.SaveAllInfo();

    }
    #endregion

    public int enemySetAct_Int = 0;
    public int login_int = 0;
    public string name_server;
    public int bestScore;
    public bool archeryreset = false;

    public bool shot = false;
    public int shot_int = 0;

    public bool cor_allowed = false;
    public bool run;

    public string userid_server;

    public bool gameoverOnOff = false;

    public int overType_int = 0;

    public Vector2 bannerStart_pos;

    public RectTransform banner_trans;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("GameManager.instance.run" + run);
    }
}
