﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankManager : MonoBehaviour
{
    PlayFabManager PF;
    public GameObject PF_obj;

    public Text name_txt_start;
    public GameObject nameSet;
    public GameObject rankSet;
    public int bestScore_server;
    public Text currentRank_txt;
    public Button name_set_btn;
    public GameObject name_Canv;

    // Start is called before the first frame update
    void Start()
    {
        PF = PF_obj.GetComponent<PlayFabManager>();
        //GameManager.instance.userid_server  = "";
        //GameManager.instance.name_server = "";
        //GameManager.instance.login_int = 0;
        //PlayerPrefs.SetInt("_BestMeters", 0);
        //SaveInfo.SaveAllInfo();
        LoadInfo.LoadAllInfo();



        if (GameManager.instance.login_int == 0)
        {           
            nameSet.SetActive(true);
        }
        else
        {

            Invoke("InvokeLoginNormal", 0.1f);

            //PF.LoginBtn();
            //nameSet.SetActive(false);
        }
    }

    void InvokeLoginNormal()
    {
        PF.LoginBtn();
        nameSet.SetActive(false);
    }

    public void NameButtonOnOff()
    {
        if (nameSet.activeSelf && Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (name_txt_start.text.Length <= 5 && 2 < name_txt_start.text.Length) name_set_btn.interactable = true;
            else name_set_btn.interactable = false;
        }
    }
       
    // Update is called once per frame
    void Update()
    {
        NameButtonOnOff();
    }

    public void StartNameSet()
    {
        string name_sub = name_txt_start.text;
        GameManager.instance.name_server = name_sub;

        PF.LoginMain(name_sub);
        nameSet.SetActive(false);
        //anima.SetBool("showStart", true);
        GameManager.instance.login_int = 1;
        SaveInfo.SaveAllInfo();
        //AC.NendOn();
        //play.SetActive(true);

    }
    /*
    void InvokeNameSet()
    {

    }*/

    public void ShowRanking()
    {
        rankSet.SetActive(!rankSet.activeSelf);

    }

    public void SetRanking(int num)
    {
        PF.SetRank(num);
    }


    public void InvokeOver()
    {
        PF.GetStat();
        //Invoke("InvokeSet",1f);
    }

    public void InvokeSet()
    {
        RankSet();
    }

    public void RankSet()
    {
        currentRank_txt.text = PF.result_rank.ToString();
    }
}
